package com.example.kitchendisplay;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.kitchendisplay.Adapter.OrderAdapter;
import com.example.kitchendisplay.Model.Orders;
import com.example.kitchendisplay.OnClick.OrderClick;
import com.example.kitchendisplay.Remote.RetrofitClient;
import com.example.kitchendisplay.Service.IpService;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayActivity extends AppCompatActivity implements OrderClick{

    private DatabaseReference mDbRef;
    private FirebaseDatabase mDatabase;
    private static RecyclerView recyclerView;
    private static OrderAdapter orderAdapter;
    Context mContext;
    ArrayList<Orders> ordersArrayList;
    private static StaggeredGridLayoutManager staggeredGridLayoutManager;
    OrderClick orderClick;
    IpService mService;
    LinearLayout orderStatus,orderPreparingStatus,orderReadyStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        Toolbar categoryToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(categoryToolbar);
        setTitle("Orders");

        orderStatus = findViewById(R.id.order_status);
        orderPreparingStatus = findViewById(R.id.preparing);
        orderReadyStatus = findViewById(R.id.ready);

        mContext = this;
        mDatabase = FirebaseDatabase.getInstance();
        mDbRef = mDatabase.getReference("Orders");

        orderClick = this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        recyclerView = findViewById(R.id.product_grid_view);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        ordersArrayList = new ArrayList<>();

        mDbRef.addChildEventListener(childListener);

    }


    ChildEventListener childListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Log.e("FIREBASE", "onChildAdded: " + dataSnapshot.getChildrenCount());
            Orders orders = dataSnapshot.getValue(Orders.class);
            orders.setOrder_id(dataSnapshot.getKey());
            if(!orders.getOrder_status().equalsIgnoreCase("3")) {
                ordersArrayList.add(orders);

                recyclerView.setLayoutManager(null);
                recyclerView.setLayoutManager(staggeredGridLayoutManager);
                orderAdapter = new OrderAdapter(mContext, ordersArrayList,orderClick);
                recyclerView.setAdapter(orderAdapter);
                orderAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            Log.e("FIREBASE", "onChildChanged: ");
            Orders orders = dataSnapshot.getValue(Orders.class);

            for(int i=0; i<ordersArrayList.size(); i++){
                if(ordersArrayList.get(i).getOrder_id() == dataSnapshot.getKey()){
                    ordersArrayList.get(i).setOrder_status(orders.getOrder_status());
                    if(orders.getOrder_status().equalsIgnoreCase("3")) {
                        ordersArrayList.remove(i);
                    }
                    orderAdapter.updateList(ordersArrayList);

                }
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            Log.e("FIREBASE", "onChildRemoved: ");
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };


    @Override
    public void onOrderItemClick(int position, final String orderId) {
        Log.e("onOrderItemClick", "onOrderItemClick: " + orderId );

        orderStatus.setVisibility(View.VISIBLE);

        orderStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderStatus.setVisibility(View.GONE);

            }
        });

        orderPreparingStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderStatus.setVisibility(View.GONE);

                mDbRef.child(orderId).child("order_status").setValue("2");

                HashMap<String, String> stringHashMap = new HashMap<>();
                stringHashMap.put("order_status", "2");
                stringHashMap.put("order_id", orderId);

                mService.changeOrderStatus(stringHashMap).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Log.e("changeOrderStatus", "onResponse: " + response.message() );
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e("changeOrderStatus", "onFailure: " + t.getMessage() );

                    }
                });
            }
        });

        orderReadyStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderStatus.setVisibility(View.GONE);

                mDbRef.child(orderId).child("order_status").setValue("3");

                HashMap<String, String> stringHashMap = new HashMap<>();
                stringHashMap.put("order_status", "3");
                stringHashMap.put("order_id", orderId);

                mService.changeOrderStatus(stringHashMap).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Log.e("changeOrderStatus", "onResponse: " + response.message() );
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e("changeOrderStatus", "onFailure: " + t.getMessage() );

                    }
                });
            }
        });

/*        AlertDialog.Builder builder = new AlertDialog.Builder(DisplayActivity.this);
        builder.setTitle("Order Status")
                .setMessage("Is this Order ready")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog  = builder.create();
        dialog.show();*/




    }
}