package com.example.kitchendisplay.OnClick;

public interface OrderClick {
    void onOrderItemClick(int position, String orderId);

}
