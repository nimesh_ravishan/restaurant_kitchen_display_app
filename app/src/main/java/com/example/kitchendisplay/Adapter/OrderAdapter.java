package com.example.kitchendisplay.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kitchendisplay.Model.CartObject;
import com.example.kitchendisplay.Model.Orders;
import com.example.kitchendisplay.OnClick.OrderClick;
import com.example.kitchendisplay.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<Orders> ordersArrayList;
    private LinearLayout dynamic_list;
    private ViewGroup parent;
    OrderClick orderClick;
    public OrderAdapter(Context mContext, ArrayList<Orders> ordersArrayList, OrderClick orderClick){
        this.mContext=mContext;
        this.ordersArrayList=ordersArrayList;
        this.orderClick=orderClick;

    }

    public void updateList(ArrayList<Orders> ordersArrayList){
        this.ordersArrayList = ordersArrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OrderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.orders_layout, parent, false);
        final MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderAdapter.MyViewHolder holder, final int position) {
        dynamic_list.removeAllViews();


        String products = ordersArrayList.get(position).getProducts();
        Gson gson = new Gson();
        Type productsListType = new TypeToken<ArrayList<CartObject>>() {
        }.getType();
        ArrayList<CartObject> productsArray = gson.fromJson(products, productsListType);

        String orderId = ordersArrayList.get(position).getOrder_id();
        String order = orderId.substring(0,6);

        holder.textOrderId.setText("ORDER ID : "+order);

        for (int i = 0; i < productsArray.size(); i++) {

            String quantity = String.valueOf(productsArray.get(i).getCartProductQuantity());
            String productName = productsArray.get(i).getCartProductName();
            String productImage = productsArray.get(i).getCartProductImage();

            View productView = null;
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            productView = inflater.inflate(R.layout.product_item_list_layout, parent, false);

            TextView textProductQuantity = productView.findViewById(R.id.list_product_qty);
            TextView textProductName = productView.findViewById(R.id.list_product_name);
            final ImageView imgProduct = productView.findViewById(R.id.list_product_image);
            final ProgressBar imgProgress = productView.findViewById(R.id.img_progress);

            if (productView.getParent() != null) {
                ((ViewGroup) productView.getParent()).removeView(productView);
            }

            Picasso.get().load(productImage)
                    .into(imgProduct, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                           imgProgress.setVisibility(View.GONE);
                           imgProduct.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError(Exception e) {
                           imgProduct.setVisibility(View.VISIBLE);

                        }

                    });

            textProductName.setText(productName);
            textProductQuantity.setText("QTY : " + quantity);

            dynamic_list.addView(productView);

        }
            holder.textOrderCount.setText(String.valueOf(productsArray.size()));


        if(ordersArrayList.get(position).getOrder_type().equalsIgnoreCase("")){
            holder.textOrderType.setText("Dine In");

        }else{
            holder.textOrderType.setText("Take Away");

        }

        if(ordersArrayList.get(position).getOrder_status().equalsIgnoreCase("1")){
            holder.orderStatus.setText("QUEUE");

        }else if(ordersArrayList.get(position).getOrder_status().equalsIgnoreCase("2")){
            holder.orderStatus.setText("PREPARING");

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderClick.onOrderItemClick(position, ordersArrayList.get(position).getOrder_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return ordersArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textOrderId,textOrderCount,textOrderType,orderStatus;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textOrderId = itemView.findViewById(R.id.order_client_name);
            textOrderCount = itemView.findViewById(R.id.order_item_count);
            textOrderType = itemView.findViewById(R.id.order_type);
            orderStatus = itemView.findViewById(R.id.order_status_text);
            dynamic_list = itemView.findViewById(R.id.dynamic_list_product);

        }
    }
}
