package com.example.kitchendisplay.Utills;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.example.kitchendisplay.R;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.util.Objects;

public class Utills {
    private static AlertDialog dialog;
    private static AccountHeader headerResult = null;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isNetworkNotAvailable(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();
        return activeNetwork == null || !activeNetwork.isConnected();
    }


    public static void insetToken(String loyaltyPointID, String loyaltyPointName, Context mContext) {
        SharedPreferences loyaltyPreferences = mContext.getSharedPreferences("authToken", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loyaltyPreferences.edit();
        editor.putString("token", loyaltyPointID);
        editor.apply();
    }

    public static String getToken(Context mContext) {
        SharedPreferences authPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.PREF_AUTH_TITLE), Context.MODE_PRIVATE);
        return authPreferences.getString("token", "");
    }

    public static boolean getUserIsLogged(Context mContext) {
        SharedPreferences userPref = mContext.getSharedPreferences(mContext.getResources().getString(R.string.PREF_AUTH_TITLE), Context.MODE_PRIVATE);
        return userPref.getBoolean(mContext.getResources().getString(R.string.IS_PREF_AUTH_LOGGED), false);
    }

    public static String getUserID(Context mContext) {
        SharedPreferences userPref = mContext.getSharedPreferences(mContext.getResources().getString(R.string.PREF_AUTH_TITLE), Context.MODE_PRIVATE);
        return userPref.getString(mContext.getResources().getString(R.string.PREF_AUTH_USER_ID), "");
    }


    public static void dialogBox(String title, String message, Context context) {

        android.app.AlertDialog.Builder builderSingle = new android.app.AlertDialog.Builder(context);
        builderSingle.setIcon(R.drawable.logo);
        builderSingle.setTitle(title);
        builderSingle.setMessage(message);
        builderSingle.setCancelable(false);

        builderSingle.setPositiveButton(context.getResources().getString(R.string.ok_btn), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.show();
    }

    public static void progressBar(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View customLayout = inflater.inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    public static Double convertDoubleToDecimalPointValue(Double value) {
        return Math.round(value * 100.00) / 100.00;
    }

    public static Double convertStringToDecimalPointValue(String value) {
        return Math.round(Double.parseDouble(value) * 100.00) / 100.00;
    }

    public static void progresBarDismiss() {
        dialog.dismiss();
    }


    public static AccountHeader buildHeader(boolean compact, Bundle savedInstanceState, final Activity mActivity, final Context mContext) {
//        UserObject mUserObject = getUserPreferences(mContext);

        IProfile userProfile1 = new ProfileDrawerItem().withNameShown(true)
                .withName("Nimesh")
                .withEmail("nimesh@gmail.com")
                .withTag("user")
                .withIdentifier(1001)
                .withTextColor(mContext.getResources().getColor(android.R.color.white))
                /*.withIcon(mipmap.kd_drawer_icon_round)*/;


        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(mActivity)
                .withTextColor(mContext.getResources().getColor(R.color.md_white_1000))
                .withTranslucentStatusBar(true)
                .withCompactStyle(compact)
                .addProfiles(
                        userProfile1
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .withCurrentProfileHiddenInList(true)
                .withSavedInstance(savedInstanceState)
                .withHeaderBackground(android.R.color.black)
                .withSelectionListEnabled(false)
//                .withProfileImagesVisible(false)
                .build();


        return headerResult;

    }

    //     add items to drawer font side
    public static Drawer drawerFront1(Toolbar toolbar, Bundle savedInstanceState, final Activity mActivity, final Context mContext) {
        final Drawer newDrawer;
        newDrawer = new DrawerBuilder()
                .withActivity(mActivity)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult) // set the AccountHeader we created earlier for the header
                .addDrawerItems(
                ) // add the items we want to use with our Drawer
                .addStickyDrawerItems(

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch (position) {
                            case 1:
//                                mContext.startActivity(new Intent(mContext, DashboardActivity.class));

                                break;
                            case 2:
//                                Intent categoryIntent = new Intent(mContext, CategoryActivity.class);
//                                mContext.startActivity(categoryIntent);
//                                drawerItem.withSetSelected(true);

                                break;
                                case 3:
//                                Intent productIntent = new Intent(mContext, ProductActivity.class);
//                                mContext.startActivity(productIntent);
//                                drawerItem.withSetSelected(true);
                                break;


                        }
                        return false;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withActionBarDrawerToggleAnimated(true)
                .withSavedInstance(savedInstanceState)
                .withDisplayBelowStatusBar(true)
                .build();

        PrimaryDrawerItem dashboard;

        dashboard = new PrimaryDrawerItem()
                .withName("Dashboard")
//                .withIcon(mActivity.getResources().getDrawable(R.drawable.dashboard))
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
//                        mActivity.startActivity(new Intent(mActivity, DashboardActivity.class));
                        return false;
                    }
                })
                .withIdentifier(1);
        newDrawer.addItem(dashboard);

        PrimaryDrawerItem category;
        category = new PrimaryDrawerItem()
                .withName("Category")
//                .withIcon(mActivity.getResources().getDrawable(R.drawable.category))
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
//                        mActivity.startActivity(new Intent(mActivity, CategoryActivity.class));
                        return false;
                    }
                })
                .withIdentifier(2);
        newDrawer.addItem(category);

        PrimaryDrawerItem product;
        product = new PrimaryDrawerItem()
                .withName("Product")
//                .withIcon(mActivity.getResources().getDrawable(R.drawable.product))
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
//                        mActivity.startActivity(new Intent(mActivity, ProductActivity.class));
                        return false;
                    }
                })
                .withIdentifier(3);
        newDrawer.addItem(product);


        return newDrawer;

    }

    public static void setProfilesToDrawerHeader(Context mContext, final Drawer mainDrawer) {
        try {
            ProfileDrawerItem userProfile1 = new ProfileDrawerItem()
                    .withName("n")
                    .withNameShown(true)
                    .withEmail("n")
                    .withTag("")
                    .withIdentifier(10)
                    .withTextColor(mContext.getResources().getColor(android.R.color.white));
        } catch (Exception e) {
            Log.e("setProfileImage", e.toString());
        }
    }
}
