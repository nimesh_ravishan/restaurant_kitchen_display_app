package com.example.kitchendisplay.Service;

import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface IpService {

    //Login
    @FormUrlEncoded
    @POST("Auth/adminLogin")
    Call<JsonObject> signUser(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("order/changeOrderStatus")
    Call<JsonObject> changeOrderStatus(@FieldMap Map<String,String> params);

}
