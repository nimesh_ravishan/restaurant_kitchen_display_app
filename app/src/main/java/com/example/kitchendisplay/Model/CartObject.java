package com.example.kitchendisplay.Model;

public class CartObject {
     String cartProductId;
     String cartProductName;
     int cartProductQuantity;
     float cartProductPrice;
     String cartProductOffer;
     String cartProductComment;
     String cartProductImage;

    public String getCartProductImage() {
        return cartProductImage;
    }

    public void setCartProductImage(String cartProductImage) {
        this.cartProductImage = cartProductImage;
    }

    public String getCartProductName() {
        return cartProductName;
    }

    public void setCartProductName(String cartProductName) {
        this.cartProductName = cartProductName;
    }

    public String getCartProductId() {
        return cartProductId;
    }

    public void setCartProductId(String cartProductId) {
        this.cartProductId = cartProductId;
    }

    public int getCartProductQuantity() {
        return cartProductQuantity;
    }

    public void setCartProductQuantity(int cartProductQuantity) {
        this.cartProductQuantity = cartProductQuantity;
    }

    public float getCartProductPrice() {
        return cartProductPrice;
    }

    public void setCartProductPrice(float cartProductPrice) {
        this.cartProductPrice = cartProductPrice;
    }

    public String getCartProductOffer() {
        return cartProductOffer;
    }

    public void setCartProductOffer(String cartProductOffer) {
        this.cartProductOffer = cartProductOffer;
    }

    public String getCartProductComment() {
        return cartProductComment;
    }

    public void setCartProductComment(String cartProductComment) {
        this.cartProductComment = cartProductComment;
    }
}

