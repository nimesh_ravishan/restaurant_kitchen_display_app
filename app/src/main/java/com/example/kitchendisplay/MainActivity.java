package com.example.kitchendisplay;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kitchendisplay.Remote.RetrofitClient;
import com.example.kitchendisplay.Service.IpService;
import com.example.kitchendisplay.Utills.Utills;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextInputEditText emailView, passwordView;
    private TextInputLayout mEmailTextInputLayout, mPasswordTextInputLayout;
    private Button signIn;
    Context mContext;
    String mEmail, mPassword;
    LinearLayout mLogin, mNetworkErrorSplash;
    IpService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        final Activity mActivity = MainActivity.this;

        mService = new RetrofitClient().getClient(this).create(IpService.class);

        emailView = findViewById(R.id.email_edittext);
        passwordView = findViewById(R.id.password_edittext);
        signIn = findViewById(R.id.sign_in_button);

        mNetworkErrorSplash = findViewById(R.id.networkErrorSplash);
        mLogin = findViewById(R.id.dataLayoutLogin);


        mEmailTextInputLayout = findViewById(R.id.email_TextInputLayout);
        mPasswordTextInputLayout = findViewById(R.id.password_TextInputLayout);
        TextView reloadScreenSplash = findViewById(R.id.go_online_network_splash);


        signIn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                if (!Utills.isNetworkNotAvailable(mContext)) {

                    View focusView = null;
                    boolean cancel = false;

                    mEmail = emailView.getText().toString().trim();
                    mPassword = passwordView.getText().toString().trim();

                    if (TextUtils.isEmpty(mPassword)) {
                        mPasswordTextInputLayout.setErrorEnabled(true);
                        mPasswordTextInputLayout.setError(getString(R.string.error_field_required));
                        focusView = passwordView;
                        cancel = true;
                    }
                    if (TextUtils.isEmpty(mEmail)) {
                        mEmailTextInputLayout.setErrorEnabled(true);
                        mEmailTextInputLayout.setError(getString(R.string.error_field_required));
                        focusView = emailView;
                        cancel = true;
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
                        mEmailTextInputLayout.setErrorEnabled(false);
                        mEmailTextInputLayout.setError(getString(R.string.error_invalid_email));
                        focusView = emailView;
                        cancel = true;
                    } else {

                        Utills.progressBar(mContext);

                        HashMap<String, String> stringHashMap = new HashMap<>();
                        stringHashMap.put("email", mEmail);
                        stringHashMap.put("password", mPassword);
                        stringHashMap.put("user_role", "3");


                        mService.signUser(stringHashMap).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                Log.e("**LOGIN**", "onResponse: " + response.body());
                                if (response.code() == 200) {
                                    Utills.progresBarDismiss();
                                    if(response.body() != null){
                                        try {
                                            JSONObject jsonObject = new JSONObject(response.body().toString());
                                            SharedPreferences userPref = getApplicationContext().getSharedPreferences(getApplicationContext().getResources().getString(R.string.PREF_AUTH_TITLE), Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editors = userPref.edit();
                                            editors.putBoolean(getApplicationContext().getResources().getString(R.string.IS_PREF_AUTH_LOGGED),true);
                                            String userName = jsonObject.getJSONArray("data").getJSONObject(0).getString("first_name") + jsonObject.getJSONArray("data").getJSONObject(0).getString("last_name");
                                            editors.putString(getApplicationContext().getResources().getString(R.string.PREF_AUTH_USER_ID),  jsonObject.getJSONArray("data").getJSONObject(0).getString("user_id"));
                                            editors.putString(getApplicationContext().getResources().getString(R.string.PREF_AUTH_EMAIL),  jsonObject.getJSONArray("data").getJSONObject(0).getString("email"));
                                            editors.putString(getApplicationContext().getResources().getString(R.string.PREF_AUTH_USERNAME),userName);
                                            editors.apply();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Log.e("RESPONSEBODY", "onResponse: "+response.body() );
                                    }
                                    Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    if (response.message() != null) {
                                        Utills.progresBarDismiss();
                                        Utills.dialogBox(getString(R.string.error_title), response.message(), mContext);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Utills.progresBarDismiss();
                                Log.e("**LOGIN**", "onFailure: " + t);

                            }
                        });

                    }
                } else {
                    showNetworkError(false);
                }
            }
        });






        reloadScreenSplash.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if (!Utills.isNetworkNotAvailable(mContext)) {
                    mLogin.setVisibility(View.VISIBLE);
                    mNetworkErrorSplash.setVisibility(View.GONE);

                } else {
                    showNetworkError(false);
                }
            }
        });
    }

    private void showNetworkError(boolean isConnected) {
        if (isConnected) {
            mLogin.setVisibility(View.VISIBLE);
            mNetworkErrorSplash.setVisibility(View.GONE);
        } else {
            mLogin.setVisibility(View.GONE);
            mNetworkErrorSplash.setVisibility(View.VISIBLE);
        }
    }

}