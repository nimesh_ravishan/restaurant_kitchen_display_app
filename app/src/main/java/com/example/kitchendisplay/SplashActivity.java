package com.example.kitchendisplay;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.kitchendisplay.Utills.Utills;

public class SplashActivity extends AppCompatActivity {

    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;

        if(!Utills.getUserIsLogged(mContext)){
            Intent login = new Intent(SplashActivity.this,MainActivity.class);
            startActivity(login);
            finish();
        }else{
            Intent dashboard = new Intent(SplashActivity.this,DisplayActivity.class);
            startActivity(dashboard);
            finish();
        }

    }
}